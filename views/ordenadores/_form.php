<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Ordenadores */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ordenadores-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'error')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'portatil')->textInput() ?>

    <?= $form->field($model, 'ncliente')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
