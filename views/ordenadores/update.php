<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ordenadores */

$this->title = 'Update Ordenadores: ' . $model->n_averia;
$this->params['breadcrumbs'][] = ['label' => 'Ordenadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->n_averia, 'url' => ['view', 'id' => $model->n_averia]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ordenadores-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
