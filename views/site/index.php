<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
$this->title = 'Control de Taller';
?>


    <div id="myCarousel" class="carousel slide">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      
      </ol>
      <div class="carousel-inner">
        <div class="item active">
          <?= Html::img('@web/images/horario.jpg', ['alt'=>'proveedores'], ['class'=>'img-circle']);?>
          <div class="container">
            <div class="carousel-caption">
              <!--
              <h1>Example headline.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-large btn-primary" href="#">Sign up today</a></p>
              -->
            </div>
          </div>
        </div>
        <div class="item">
          <?= Html::img('@web/images/horario.jpg', ['alt'=>'proveedores'], ['class'=>'img-circle']);?>
          <div class="container">
            <div class="carousel-caption">
              <!--
              <h1>Another example headline.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-large btn-primary" href="#">Learn more</a></p>
              -->
            </div>
          </div>
        </div>
        <div class="item">
          <?= Html::img('@web/images/horario.jpg', ['alt'=>'proveedores'], ['class'=>'img-circle']);?>
          <div class="container">
            <div class="carousel-caption">
              <!--
              <h1>One more for good measure.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-large btn-primary" href="#">Browse gallery</a></p>
              -->
            </div>
          </div>
        </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
    </div><!-- /.carousel -->



    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container marketing">

      <!-- Three columns of text below the carousel -->
      <div class="row">
        <div class="col-lg-4">
             <h2>Proveedores</h2>
              <br> 
           <?= Html::img('@web/images/proveedores.jpg', ['alt'=>'proveedores'], ['class'=>'img-circle']);?>
          <br>
          <br>
          
          
          
          
          
          <?= Html::a('Revisar', ['site/consulta10a'], ['class' => 'btn btn-primary'])?>
        
        </div><!-- /.col-lg-4 -->
        
        
        <div class="col-lg-4">
            <h2>Stock</h2>
            <br>
         <?= Html::img('@web/images/stock.jpg', ['alt'=>'proveedores'], ['class'=>'img-circle']);?>
          <br>
          <br>
          <?= Html::a('Revisar', ['site/consulta2a'], ['class' => 'btn btn-primary'])?>
         
        </div><!-- /.col-lg-4 -->
        
        <div class="col-lg-4">
             <h2>Averias</h2>
              <br>
         <?= Html::img('@web/images/averias.jpg', ['alt'=>'proveedores'], ['class'=>'img-circle']);?>
           <br>
          <br>
                 
                 
          <?= Html::a('Revisar', ['site/consulta3a'], ['class' => 'btn btn-primary'])?>
           </div><!-- /.col-lg-4 -->

      </div><!-- /.row -->


      <!-- START THE FEATURETTES -->

      <hr class="featurette-divider">
    <div class="row featurette">
        <div class="col-md-7">
          <h2 class="featurette-heading">BackEnd de nuestro Taller</h2>
          <p class="lead">Pagina destinada a el control del almacenamiento y actuacion de Talleres Icarus</p>
        </div> 
      <div class="col-md-5">
           <?= Html::img('@web/images/Logo.png', ['alt'=>'Logo'], ['class'=>'img-circle']);?>
        </div>

    


      <!-- FOOTER -->
      <footer>
        <p class="pull-right"><a href="#">Back to top</a></p>
        <p>&copy; 2020 Talleres, Inc. &middot; <a href="#">Privacy</a> 
      </footer>

    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./js/jquery.js"></script>
    <script src="./js/bootstrap.min.js"></script>
    <script src="./js/holder.js"></script>

