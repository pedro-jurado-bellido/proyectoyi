<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<!DOCTYPE html>

 

    
    <div id="myCarousel" class="carousel slide">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      
      </ol>
      <div class="carousel-inner">
        <div class="item active">
          <?= Html::img('@web/images/horario.jpg', ['alt'=>'proveedores'], ['class'=>'img-circle']);?>
          <div class="container">
            <div class="carousel-caption">
              <!--
              <h1>Example headline.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-large btn-primary" href="#">Sign up today</a></p>
              -->
            </div>
          </div>
        </div>
          
          <br/>
    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

     <!-- Three columns of text below the carousel -->
       <div class="container marketing">

      <!-- Three columns of text below the carousel -->
      <div class="row">
        <div class="col-lg-4">
             <h2>Proveedores</h2>
              <br> 
           <?= Html::img('@web/images/proveedores.jpg', ['alt'=>'proveedores'], ['class'=>'img-circle']);?>
          <br>
          <br>
          
          
          
          
          
          <?= Html::a('Revisar', ['site/consulta10a'], ['class' => 'btn btn-primary'])?>
        
        </div><!-- /.col-lg-4 -->
        
        
        <div class="col-lg-4">
            <h2>Stock</h2>
            <br>
         <?= Html::img('@web/images/stock.jpg', ['alt'=>'proveedores'], ['class'=>'img-circle']);?>
          <br>
          <br>
          <?= Html::a('Revisar', ['site/consulta2a'], ['class' => 'btn btn-primary'])?>
         
        </div><!-- /.col-lg-4 -->
        
        <div class="col-lg-4">
             <h2>Averias</h2>
              <br>
         <?= Html::img('@web/images/averias.jpg', ['alt'=>'proveedores'], ['class'=>'img-circle']);?>
           <br>
          <br>
                 
                 
          <?= Html::a('Revisar', ['site/consulta3a'], ['class' => 'btn btn-primary'])?>
           </div>
   
        <div class="col-lg-4">
            <h2>Empleados</h2>
            <br>
            <?= Html::img('@web/images/Empleados.png', ['alt'=>'Empleados'], ['class'=>'img-circle']);?> 
            <br>
            <br>
          <?= Html::a('Revisar', ['site/consulta4a'], ['class' => 'btn btn-primary'])?>
          
        </div><!-- /.col-lg-4 -->

      
       <div class="col-lg-4">
           <h2>Clientes</h2>
           <br>
        <?= Html::img('@web/images/Cliente.png', ['alt'=>'Clientes'], ['class'=>'img-circle']);?> 
           <br>
           <br>
          <?= Html::a('Revisar', ['site/consulta5a'], ['class' => 'btn btn-primary'])?>
          
        </div><!-- /.col-lg-4 -->
        
         <div class="col-lg-4">
             <h2>Pedidos</h2>
             <br>
         <?= Html::img('@web/images/compras.png', ['alt'=>'Arreglos'], ['class'=>'img-circle']);?> 
          <br>
           <br>
          <?= Html::a('Revisar', ['site/consulta6a'], ['class' => 'btn btn-primary'])?>
          
        </div><!-- /.col-lg-4 -->
   
  </div><!-- /.row -->
       </div>


      <!-- START THE FEATURETTES -->

      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-7">
          <h2 class="featurette-heading">BackEnd de nuestro Taller</h2>
          <p class="lead">Pagina destinada a el control del almacenamiento y actuacion de Talleres Icarus </p>
        </div> 
        <div class="col-md-5">
           <?= Html::img('@web/images/Logo.png', ['alt'=>'Logo'], ['class'=>'img-circle']);?>
        </div>
      </div>

    


      <!-- FOOTER -->
      <footer>
        <p class="pull-right"><a href="#">Back to top</a></p>
        <p>&copy; 2020 Talleres, Inc. &middot; </p>
      </footer>

    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./js/jquery.js"></script>
    <script src="./js/bootstrap.min.js"></script>
    <script src="./js/holder.js"></script>

