<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Arreglo */

$this->title = 'Create Arreglo';
$this->params['breadcrumbs'][] = ['label' => 'Arreglos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="arreglo-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
