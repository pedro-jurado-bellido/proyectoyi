<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Arreglo */

$this->title = 'Update Arreglo: ' . $model->n_averia;
$this->params['breadcrumbs'][] = ['label' => 'Arreglos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->n_averia, 'url' => ['view', 'id' => $model->n_averia]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="arreglo-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
