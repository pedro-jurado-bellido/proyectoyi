<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pedir */

$this->title = 'Create Pedir';
$this->params['breadcrumbs'][] = ['label' => 'Pedirs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pedir-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
