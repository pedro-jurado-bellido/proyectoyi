<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pedir".
 *
 * @property int $id_empresa
 * @property int $cod_empleado
 */
class Pedir extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pedir';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_empresa', 'cod_empleado'], 'required'],
            [['id_empresa', 'cod_empleado'], 'integer'],
            [['id_empresa'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_empresa' => 'Id Empresa',
            'cod_empleado' => 'Cod Empleado',
        ];
    }
}
