<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cliente".
 *
 * @property int $ncliente
 * @property string|null $nombre
 * @property int|null $compra
 * @property int|null $cod_empleado
 */
class Cliente extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cliente';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['compra', 'cod_empleado'], 'integer'],
            [['nombre'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ncliente' => 'Ncliente',
            'nombre' => 'Nombre',
            'compra' => 'Compra',
            'cod_empleado' => 'Cod Empleado',
        ];
    }
}
