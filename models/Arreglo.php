<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "arreglo".
 *
 * @property int $n_averia
 * @property int $cod_empleado
 */
class Arreglo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'arreglo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_empleado'], 'required'],
            [['cod_empleado'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'n_averia' => 'n_averia',
            'cod_empleado' => 'cod_empleado',
        ];
    }
}
