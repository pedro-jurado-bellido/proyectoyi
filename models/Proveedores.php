<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proveedores".
 *
 * @property string $nombre
 * @property string|null $localizacion
 * @property int|null $id_empresa
 */
class Proveedores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proveedores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['id_empresa'], 'integer'],
            [['nombre'], 'string', 'max' => 50],
            [['localizacion'], 'string', 'max' => 15],
            [['nombre'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nombre' => 'Nombre',
            'localizacion' => 'Localizacion',
            'id_empresa' => 'Id Empresa',
        ];
    }
}
