<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ordenadores".
 *
 * @property int $n_averia
 * @property string|null $Error
 * @property int|null $portatil
 * @property int $ncliente
 */
class Ordenadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ordenadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['portatil', 'ncliente','n_averia'], 'integer'],
            [['ncliente'], 'required'],
            [['error'], 'string', 'max' => 50],
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'n_averia' => 'N_averia',
            'error' => 'Error',
            'portatil' => 'Portatil',
            'ncliente' => 'Ncliente',
        ];
    }
}
