<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "materiales".
 *
 * @property string $tipo
 * @property int|null $cantidad
 * @property int|null $id_empresa
 */
class Materiales extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'materiales';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tipo'], 'required'],
            [['cantidad', 'id_empresa'], 'integer'],
            [['tipo'], 'string', 'max' => 15],
            [['tipo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'tipo' => 'Tipo',
            'cantidad' => 'Cantidad',
            'id_empresa' => 'Id Empresa',
        ];
    }
}
